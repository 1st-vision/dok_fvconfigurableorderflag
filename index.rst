.. |label| replace:: Konfigurierbares Bestell-Kennzeichen
.. |snippet| replace:: FvConfigurableOrderFlag
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.11
.. |maxVersion| replace:: 5.3.4
.. |version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin ermöglicht eine konfigurierbare Markierung von Bestellungen auf Basis der bestellten Artikel. Es kann somit mit frei definierbaren Bedingungen festgelegt werden, wann eine Bestellung markiert werden soll. Markierung bedeutet, dass die Bestellung ein festes Attributfeld hat, das vom Plugin individuell gesetzt wird. Auf dieses Attributfeld kann dann im Mapping des Connectors zugegriffen werden.

Als Beispiel für eine Verwendungsmöglichkeit des Plugins wäre beispielsweise ein Anfrage-System. Einige Artikel des Shops können aufgrund bestimmter Liefervoraussetzungen o.ä. nur angefragt werden. Sobald einer dieser Artikel in der Bestellung vorhanden ist, muss diese Bestellung als Anfrage behandelt werden und nicht als Bestellung. Im Mapping kann hierbei nun überprüft werden, ob die Bestellung entsprechend markiert ist, und falls ja, wird eine andere Belegart exportiert.


Frontend
--------



Backend
-------
.. image:: FvConfigurableOrderFlag1.png

Die Plugin-Konfiguration kann subshop-spezifisch definiert werden.

:Positions-Bedingung Attributfeld: Das Attributfeld, das für die Positions-Bedingung hergenommen wird. Möglich sind alle Felder der Tabelle s_articles_attributes. Diese werden bei der Installation neu eingelesen, daher ist bei Anlage von neuen Attributfeldern in s_articles_attributes eine Neuinstallation des Plugins indiziert.
:Positions-Bedingung: Die Bedingung zum Setzen des Basket-Attribut-Flags. Bei Auswahl „Eigene Bedingung (PHP)“ werden die Felder „Positions-Bedingung: Attributfeld“ und „Positions-Bedingung: STRING“ ignoriert.
:Positions-Bedingung STRING: Die Zeichenkette, mit der das Attributfeld verglichen wird, sofern als „Positions-Bedingung“ entsprechendes ausgewählt wurde.
:Position: eigene Bedingung: PHP-Code der bei Auswahl der entsprechenden „Positions-Bedingung“ ausgeführt wird und darüber entscheidet, was in das Basket-Attribut-Flag geschrieben wird.

Verfügbare Umgebungvariablen für Positionen:
$s_articles_attributes_*
$s_articles_details_*
$s_articles_*
$s_order_basket_*

Beispiel: $s_articles_attributes_attr4 entspricht dem Datenbankfeld s_articles_attributes.attr4

:Bestell-Bedingung: Die Bedingung zum Setzen des Order-Attribut-Flags.
:Bestellung eigene Bedingung: PHP-Code der bei Auswahl der entsprechenden „Positions-Bedingung“ ausgeführt wird und darüber entscheidet, was in das Basket-Attribut-Flag geschrieben wird.

Verfügbare Umgebungvariablen für Bestellung:

.. image:: FvConfigurableOrderFlag2.png

Hierbei muss über das $positions-Array iteriert werden, das alle Basket-Positionen beinhaltet. Ein Beispielcode ist bereits nach Installation des Plugins eingetragen. 

Beispiel-FvOLSI-Export-Mapping
______________________________
Das Order-Attributfeld fvConfigurableFlag (VARCHAR (500)) wird zu Boolean gecastet und falls es gesetzt wurde, wird die Belegart Anfrage in der Office Line erzeugt (VPA). Andernfalls die Standard-Auftragsbestätigung (VVA).

.. image:: FvConfigurableOrderFlag3.png


technische Beschreibung
------------------------
Das Plugin greift prinzipiell an zwei Stellen in die Kaufabwicklung ein.

(1)	Beim Legen eines Artikels in den Warenkorb. Hier wird anhand gesetzter Bedingungen überprüft, ob der Artikel diese Bedingungen erfüllt oder nicht. Falls er sie erfüllt, wird in das Basket-Attributfeld fv_configurable_flag 1 oder 0 geschrieben (Ausnahme „eigene Bedingung“ mit PHP).

Sollte eine eigene Bedingung mit PHP definiert sein, so können auch andere Werte in das Basket-Attribut-Flag (VARCHAR(500)) geschrieben werden.

(2)	Beim Abschicken der Bestellung (Finish-Action des Checkouts) werden alle Positionen auf Markierung überprüft und auf Basis der „Bestell-Bedingung“ das Order-Attributfeld fv_configurable_flag auf 0 oder 1 gesetzt.

Sollte eine eigene Bedingung mit PHP definiert sein, so können auch andere Werte in das Basket-Attribut-Flag (VARCHAR(500)) geschrieben werden. Außerdem kann hier nicht nur auf die Information zugegriffen werden, ob das Basket-Flag pro Position gesetzt ist oder nicht, sondern es kann auf vielfältige Basket- und Artikel-Informationen der Positionen zugegriffen werden.


Die Überprüfung aus Punkt 2 wird auch im Warenkorb und Checkout bereits durchgeführt und das temporäre Ergebnis an das Frontend gesendet, damit daraufhin eventuelle Template-Anpassungen im Theme durchgeführt werden können (bspw. Änderungen von Textbausteine).

:Die zu verwendende Template-Variable lautet: $fvConfigurableOrderFlag



Modifizierte Template-Dateien
-----------------------------
keine


